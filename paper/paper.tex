
%%%%%%%%%%%%%%%%%%%%%%% file typeinst.tex %%%%%%%%%%%%%%%%%%%%%%%%%
%
% This is the LaTeX source for the instructions to authors using
% the LaTeX document class 'llncs.cls' for contributions to
% the Lecture Notes in Computer Sciences series.
% http://www.springer.com/lncs       Springer Heidelberg 2006/05/04
%
% It may be used as a template for your own input - copy it
% to a new file with a new name and use it as the basis
% for your article.
%
% NB: the document class 'llncs' has its own and detailed documentation, see
% ftp://ftp.springer.de/data/pubftp/pub/tex/latex/llncs/latex2e/llncsdoc.pdf
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\documentclass[runningheads,a4paper]{llncs}

\setcounter{tocdepth}{3}
\usepackage{graphicx}
\usepackage{color}
\usepackage{array}
\usepackage{multirow}
\usepackage{url}
\usepackage{microtype}
\usepackage{breakcites}
\usepackage{algpseudocode}
\usepackage{algorithm}
\usepackage{amsmath}
\usepackage{amssymb}

%\let\proof\relax
%\usepackage{amsthm}
%\let\endproof\relax

\usepackage{framed}


\newcommand*{\defeq}{\mathrel{\vcenter{\baselineskip0.5ex \lineskiplimit0pt
                     \hbox{\scriptsize.}\hbox{\scriptsize.}}}%
                     =}
\DeclareMathOperator*{\argmin}{arg\,min}
\newcommand{\BigO}[1]{\ensuremath{\operatorname{O}\left(#1\right)}}
\def\!#1{\ensuremath{\mathbf{#1}}}
\def\*#1{\ensuremath{\mathcal{#1}}}

\newcommand{\family}{\ensuremath{\*F^{\!S}_l}}
\newcommand{\locality}{\ensuremath{\ell^S_M}}

\newcommand{\shdd}{\textsc{sHDD}}
\newcommand{\hhdd}{\textsc{hHDD}}
\newcommand{\hhddp}{\textsc{hHDD-pack}}
\newcommand{\ffd}{\textsc{ffd}}
\newcommand{\opt}{\textsc{opt}}

\begin{document}

\mainmatter  % start of an individual contribution

% first the title is needed
\title{Hashing-based Hybrid Duplicate Detection for Bayesian Network Structure Learning}

% a short form should be given in case it is too long for the running head
\titlerunning{Hashing-based HDD for Bayesian Network Structure Learning}

% the name(s) of the author(s) follow(s) next
%
% NB: Chinese authors should write their first names(s) in front of
% their surnames. This ensures that the names appear correctly in
% the running heads and the author index.
%
\author{Niklas Jahnsson$^1$, Brandon Malone$^2$ and Petri Myllym\"{a}ki$^{1,3}$}
%
\authorrunning{Hashing-based HDD for Bayesian Network Struture Learning}
% (feature abused for this document to repeat the title also on left hand pages)

% the affiliations are given next; don't give your e-mail address
% unless you accept that it will be published
\institute{$^1$ University of Helsinki, $^2$ Max Planck Institute for the Biology of Ageing and $^3$~Helsinki Institute for Information Technology}

%
% NB: a more complex sample for affiliations and the mapping to the
% corresponding authors can be found in the file "llncs.dem"
% (search for the string "\mainmatter" where a contribution starts).
% "llncs.dem" accompanies the document class "llncs.cls".
%

\toctitle{Hashing-based Hybrid Duplicate Detection for Bayesian Network Structure Learning}
\tocauthor{Niklas Jahnsson, Brandon Malone and  Petri Myllym\"{a}ki}
\maketitle


\begin{abstract}
In this work, we address the well-known score-based Bayesian network structure learning problem. 
Breadth-first branch and bound (BFBnB) has been shown to be an effective approach for solving this problem. 
Delayed duplicate detection (DDD) is an important component of the BFBnB algorithm. 
Previously, an external sorting-based technique, with complexity \BigO{m \log m}, where $m$ is the number of nodes stored in memory, was used for DDD. 
In this work, we propose a hashing-based technique, with complexity \BigO{m}, for DDD. 
In practice, by removing the \BigO{\log m} overhead of sorting, over an order of magnitude more memory is available for the search.
Empirically, we show the extra memory improves locality and decreases the amount of expensive external memory operations.
We also give a bin packing algorithm for minimizing the number of external memory files.
%Empirical results suggest hashing-based DDD is much more memory-efficient. 
%Additionally, the proposed hash function could also be used to, for example, efficiently parallelize BFBnB.
\keywords{Bayesian networks, structure learning, state space search, delayed duplicate detection}
\end{abstract}

\section{Introduction}

Bayesian networks (BNs) are a widely-used formalism for capturing uncertainty among variables in a domain of interest.
When the relationship among the variables is not known \textit{a priori}, we must learn those relationships from data.
In this work, we present a novel approach to significantly reduce both the time and memory complexity for an existing structure learning algorithm.

In the \emph{score-based} framework, the BN structure learning problem (BNSL) is cast as an optimization problem in which the goal is to find a BN structure which optimizes a scoring function. 
The scoring function is typically a penalized log-likelihood function which trades off the fit of a BN to the data with its complexity. 
Even though BNSL is known to be NP-hard~\cite{Chickering1996}, many algorithms have been proposed which solve the problem exactly~\cite{Ott2004,Koivisto2004,Silander2006,Campos2011,Yuan2013,Bartlett2015,Beek2015}.

State space search algorithms have been shown to be among the state-of-the-art approaches to solving BNSL~\cite{Malone2014}.
In particular, breadth-first branch and bound (BFBnB) takes advantage of regularities in the search space  to efficiently find the optimal BN.
Nevertheless, the size of the space is still exponential in the size of the network, and, in the worst case, exploring it requires exponential time and memory.
Previous work~\cite{Malone2011} has shown that, in practice, the exponential memory requirement is especially challenging.
The algorithm can always be given more time;
however, if it exceeds the available memory resources, nothing can be done to solve the instance.

Previously~\cite{Malone2011}, a sorting-based hybrid duplicate detection (\shdd) strategy was used to allow BFBnB to efficiently use external memory, such as hard disk.
This approach uses a hash table of size $m$ to efficiently detect duplicates in the search space.
When the size of the hash table grows too large, it is sorted and written to disk;
duplicates are detected later using an external memory merge-sort operation.
Of course, sorting the hash table requires \BigO{m \log m} memory\footnote{In this work, we use ``memory'' to refer to fast-access storage, such as RAM; by ``external memory,'' we mean storage with slower access, such as hard disks and network storage. All of the theoretical complexity analysis, such as \BigO{\cdot}, refers to fast-access storage.}.
Thus, \shdd~actually requires $M = \BigO{m \log m}$ memory due to sorting.
In some sense, then, a factor of \BigO{\log m} memory is wasted.

In the heuristic search community, hashing-based delayed duplicate detection~\cite{Korf2008} has emerged as a linear-complexity alternative to sorting-based approaches.
In this approach, one hash function is used to write nodes to disk, while a second hash function is used to identify duplicates in memory.
The first hash function must ensure the number of unique nodes written to any file does not exceed memory resources.
Importantly, this hashing-based approach replaces the \BigO{m \log m} sorting  with linear-complexity operations.
%This allows it to exploit \emph{locality} much better than sHDD and perform far fewer expensive external memory operations.

In this work, we develop a hashing-based hybrid  duplicate detection (\hhdd) strategy for use in BFBnB for solving BNSL.
In particular, we construct an appropriate hash function for writing nodes to files which respects memory limitations.
Additionally, we propose a bin packing algorithm for minimzing the number of files.
Experimentally, we show that, for a fixed amount of memory, \hhdd~has better locality than \shdd~because \hhdd~does not use memory for sorting.
%We also show that our bin packing algorithm typically uses the minimum number of files possible.
We also show that much of the locality inherent in \shdd~can be preserved by expanding disk files in a particular order.

%In addition to hHDD, we also note that the hash function and bin packing algorithm can be used, largely unchanged, to efficiently parallelize the state space search approach for BNSL.

The rest of the paper is structured as follows.
In Section~\ref{sec:background}, we give background on BNSL and the state space formulation.
Section~\ref{sec:hhdd} presents our main contributions: the necessary hashing functions and bin packing algorithms for \hhdd.
We experimentally evaluate \shdd~and \hhdd~in Section~\ref{sec:experiments};
discussion in Section~\ref{sec:discussion} concludes the paper.


\section{Background}
\label{sec:background}

\subsection{Bayesian networks}

A Bayesian network~\cite{Pearl1988} is a compact representation of a joint probability distribution over the random variables $\!V = \{X_1, \dots, X_n\}$.
It consists of a directed acycic graph (DAG) in which each vertex corresponds to one of the random variables;
the edges in the graph indicate conditional independencies among the variables.
Additionally, each variable $X_i$ has an associated probability distribution, conditioned on its parents in the DAG, $PA_i$.
The joint probability distribution given by the network is
\begin{equation}
P(\!V) = \prod_i^n P(X_i | PA_i) \text{.}
\end{equation}

Given a dataset $\*D = \{D_1, \dots D_N\}$, where each $D_i$ is a complete instantiation of $\!V$, the goal of structure learning is to find a Bayesian network $\*N$ which best fits $\*D$.
The fit of $\*N$ to $\*D$ is quantified by a scoring function $s$.
Many scoring functions have been proposed in the literature, including Bayesian scores~\cite{Cooper1992,Heckerman1995}, MDL-based scores~\cite{Suzuki1999,Silander2008}, and independence-based scores~\cite{Campos2000}, among others.
The scoring functions can typically be interpretted as penalized log-likelihood functions.
All commonly used scoring functions are \emph{decomposable}~\cite{Heckerman1995};
that is, they decompose into a sum of \emph{local scores} for each variable, its parents, and the data,
\begin{equation}
s(\*N; \*D) = \sum_i^n s_i(PA_i; \*D) \text{,}
\end{equation}
where $s_i(PA_i)$ gives the score of $X_i$ using $PA_i$ as its parents and is non-negative.
We omit $\*D$ when it is clear from context.

A variety of pruning rules~\cite{Suzuki1999,Tian2000,Teyssier2005,Campos2011} can be used to demonstrate that some parent sets are never optimal for some variables.
Additionally, in practice, large parent sets are often pruned \textit{a priori}~\cite{Malone2015}.
We refer to parent sets remaining after such pruning as \emph{candidate parent sets} and denote all candidate parent sets of $X_i$ as $\*P_i$.

The \emph{Bayesian network structure learning} problem (BNSL) is then defined as follows\footnote{The problem can also be defined as a maximization using non-positive local scores.}.

\begin{framed}
\begin{minipage}{0.9\columnwidth}
\paragraph{The BNSL Problem}

\begin{description}
\item[\textsc{Input:}]
A set $\!V = \{X_1,\ldots,X_n\}$ of variables and a local score $s_i(PA_i)$ for each $PA_i \in \*P_i$ for each $X_i$.
\item[\textsc{Task:}]
Find a DAG $N^*$ such that
\begin{align*}
\label{eq:bnsl-objective}
N^* \in\argmin_{N} \sum_{i=1}^n s_i(PA_i),
\end{align*}
where $PA_i$ is the parent set of $X_i$ in $N$ and $PA_i \in \*P_i$.
\end{description}
\end{minipage}
\end{framed}


\subsection{State space search for BNSL}
\label{sec:state-space-search}

%Many techniques have been used to solve BNSL.
State space search algorithms are one state-of-the-art technique for solving BNSL~\cite{Yuan2013,Malone2014}.
Figure~\ref{fig:order-graph} shows the \emph{implicit} space for four variables.
Each node in the space corresponds to an optimal network over a subset of the variables;
we refer to nodes in the graph and subsets of the variables interchangeably.
The top-most node, containing no variables, is the $start$ node, and the bottom-most node with all variables is the $goal$ node.

An arc from \!U to $\!U \cup \{X_i\}$ in the graph indicates generating a \emph{successor} node by adding the variable $X_i$ as a leaf of the optimal subnetwork for \!U.
The cost of the arc is the score of the optimal parent set for $X_i$ out of \!U,
\begin{equation}
\label{eqn:best-score}
BestScore(X_i, \!U) = \min_{PA_i \subset \!U} s_i(PA_i) \text{.}
\end{equation}
Each node \!U stores the cost from $start$ to \!U, $g(\!U)$, which is the sum of the arc costs on the shortest path from $start$ to \!U.

\emph{Expanding} a node amounts to adding each remaining variable as a leaf of its current subnetwork.
Thus, a path in the implicit graph from $start$ to $goal$ corresponds to a total ordering over the variables;
consequently, this graph is called the \emph{order graph}.
The cost of the path exactly gives the cost of the corresponding network.
Thus, BNSL can be solved by finding the shortest path from $start$ to $goal$.

\begin{figure}[tb]
    \centering
    \includegraphics[width=0.75\textwidth,keepaspectratio]{order-graph.eps}
    \caption{The order graph for four variables \label{fig:order-graph}}
\end{figure}

Several algorithms, such as A*~\cite{Yuan2013} and depth-first search~\cite{Malone2013a}, have been used to solve this problem.
Breadth-first branch and bound~\cite{Malone2011,Fan2014} (BFBnB) has been shown to be an effective search strategy for this space.
In BFBnB, nodes are expanded in a layer-wise fashion, where a layer corresponds to all subnetworks of the same size.
The optimal network can be reconstructed using standard back-pointer techniques~\cite{Russell2003} or more memory-efficient recursive strategies~\cite{Zhou2003a}.

\textbf{Pruning.}
Much like depth-first branch and bound, BFBnB can benefit from pruning~\cite{Zhou2006}.
Before beginning the search, an upper bound $b$ on the cost of the optimal solution is found with an approximation algorithm, such as greedy hill climbing.
Then, for each node \!U, an admissible heuristic function $h$ is used to estimate the distance from \!U to the goal, $h(\!U)$.
If the sum $g(\!U) + h(\!U)$ is worse than $b$, then $\!U$ can be pruned.
For BNSL, pattern databases~\cite{Yuan2012,Fan2014} are effective admissible heuristics.
%When the upper bound $b$ is tight ($b = s(N^*)$), BFBnB is optimal in the sense that no algorithm can expand fewer nodes and prove optimality using $h$, up to tie-breaking~\cite{Zhou2006}.

\textbf{Immediate duplicate detection.}
One of the main operators of BFBnB is \emph{duplicate detection} (DD).
In the context of BNSL, duplicates are subnetworks over the same set of variables but with different orderings.
Due to Equation~\ref{eqn:best-score}, DD entails selecting the subnetwork with the minimum score.

A typical approach to DD is to use an in-memory hash table to detect nodes for the same subnetwork.
The hash table retains the best copy of a node found so far.
Since the hash table is used to detect duplicates as soon as they are generated, we refer to this strategy as \emph{immediate duplicate detection} (IDD).
For BNSL, the in-memory hash table stores, in the worst case, all of the nodes in one layer.
The largest layer of the order graph contains \BigO{C(n, \frac{n}{2})}, where $C(\cdot, \cdot)$ is the binomial coefficient.
Thus, the size of the largest layer is still exponential in the number of variables.

\textbf{Delayed duplicate detection.}
Due to the exponential worst-case memory requirement, previous work~\cite{Malone2011} augmented IDD with \emph{delayed duplicate detection} (DDD)~\cite{Korf2004}.
The essence of DDD is to use external memory, such as hard disk, to store nodes.
Then, efficient disk access techniques are used to remove duplicates from disk.

%In contrast to more general search spaces, the order graph has a very regular structure.
%In particular, all of the successors of nodes in layer $l$ are in layer $l+1$.
%Thus, for searching the order graph, BFBnB can proceed one layer at a time as follows.
%Layer $l$ is expanded, and all successors (layer $l+1$) are written to disk.
%A DDD strategy is used to remove duplicates from the disk files.
%Then, layer $l+1$ is expanded.

\subsection{Hybrid duplicate detection}
\label{sec:hdd}

The previous BFBnB algorithm for BNSL~\cite{Malone2011} combined IDD with DDD.
We refer to this combined strategy as \emph{hybrid duplicate detection} (HDD).
In particular, a sorting-based HDD (\shdd) strategy was used.
In \shdd, IDD is performed until the hash table reaches a given memory limit $m$;
then, the nodes in the hash table are sorted and written to an external memory file.
After expanding all of the nodes in one layer, DDD is performed via an external-memory merge sort operation on the disk files~\cite{Korf2004}.
As a result of the sorting, nodes for each layer are always expanded in sorted order.
The space complexity of \shdd~is dominated by sorting, which requires \BigO{m \log m} space.

An empirically important aspect of HDD is \emph{locality}.
In this context, locality refers to duplicates detected with IDD.
Specifically, we define the locality of search strategy $S$ as follows.
\begin{equation}
\locality = \frac{n_u}{n_w} \text{,}
\end{equation}
where $S$ is the HDD strategy, $M$ is the memory bound, $n_u$ is the number of unique nodes in the search space and $n_w$ is the number of nodes written to disk.
So, algorithms with a high locality remove most duplicates with IDD.
If IDD removes all duplicates, then $\locality = 1$.
In our analysis, we define the locality of HDD for a single layer of the order graph analogously.

Increasing locality reduces the time spent on expensive external memory read and write operations.
Larger in-memory hash tables (larger $m$) allow for more effective IDD and improve locality.
(We empirically verify this in Section~\ref{sec:experiments}.)

As previously mentioned, sorting the hash table before writing it to disk requires \BigO{m \log m} space.\footnote{Efficient sorting implementations, such as the \texttt{g++} version of \texttt{std::sort}, often do not exhaust the additional \BigO{\log m} space; however, it is difficult to \textit{a priori} estimate the required overhead, so \BigO{m \log m} must be used to ensure stable algorithm behavior.}.
Thus, in order to ensure sorting does not exceed the memory bound $M$, the size of the hash table is restricted to $\frac{M}{\log m}$.
So, the sorting operation impairs locality because the hash table cannot use all of the available memory.

\section{Hashing-based HDD for BNSL}
\label{sec:hhdd}

In this work, we propose to use hashing-based DDD~\cite{Korf2008} for the DDD operation in HDD.
In hashing-based DDD, rather than writing nodes to multiple sorted files, a hash function is used to distribute nodes to files.
%So, rather than creating a new, sorted file when writing nodes to disk, nodes are appended to multiple existing files.
The hash function must ensure all duplicates are written to the same file.
Additionally, it must ensure at most $M$ unique nodes are written to any single file.
After expanding all of the nodes in a layer, each external file is sequentially read back into an in-memory hash table to identify and remove duplicates.
The space complexity of hashing-based DDD is \BigO{m}.
Since hashing-based DDD avoids the \BigO{\log m} overhead of sorting, it allows the use of the entire $M$ space for the hash table.
%consequently, hashing-based DDD increases locality compared to sorting-based DDD for random node generation strategies.

In this section, we describe the hash functions necessary for hashing-based DDD for BNSL.
Additionally, we give a bin packing algorithm for minimizing the number of external memory files.

\subsection{Dividing nodes into families}
\label{sec:dividing-nodes}

The basic idea for dividing order graph nodes into files is based on the use of subcombinations, somewhat similar to the approach used by Tamada \textit{et al.}, for parallelization~\cite{Tamada2011}.
We refer to $\!S \subset \!U$ as a \emph{subcombination} of \!U.
Furthermore, when \!S contains (exactly) the lexicographically first $k$ elements of \!U, we refer to \!S as a \emph{first-$k$ subcombination} of \!U.
Additionally, when \!S is a first-$k$ subcombination of \!U, we refer to \!U as an \emph{extension} of \!S.
We denote all size-$l$ extensions of \!S as \family~(the ``family'' of \!S).
In general, \family~contains \BigO{C(n-k, l-k)} elements, where $C(\cdot, \cdot)$ is the binomial coefficient.

%The families form the basis of our approach for dividing order graph nodes into buckets.
We assign each node to the lexicographically first family to which it belongs.
For example, $\!U = \{X_1, X_2, X_3\}$ belongs to $\*F^{\{X_1, X_2\}}_3$ rather than $\*F^{\{X_1, X_3\}}_3$.
%In principle, each family will correspond to one hash bucket (see also Section~\ref{sec:distributing-buckets}).
%We use ``buckets'' and ``families'' interchangeably.

\subsection{Optimizing family size}
\label{sec:optimizing-bucket-size}

In order to minimize hashing-related overhead, we aim to minimize the number of families.
As mentioned in Section~\ref{sec:dividing-nodes}, at layer $l$ with families based on first-$k$ subcominations, the largest family contains $C(n-k, l-k)$ nodes.

As a resource restriction, we assume the in-memory hash table can store at most $m$ nodes\footnote{For this analysis, we do not consider the load factor of the hash table.}.
Thus, we select $k$ such that the largest family contains no more than $m$ nodes.
At each layer of the order graph, we select $k$ by solving
\begin{equation}
    \label{eqn:min-k}
    \argmin_k C(n-k, l-k) \le m\text{,}
\end{equation}
where $n$, $l$ and $m$ are fixed and $0<k<l$.
Empirically, we found that a linear scan of values of $k$ outperformed more sophisticated optimization strategies, such as Newton's method, for minimizing Equation~\ref{eqn:min-k}.

\subsection{Distributing families to files}
\label{sec:distributing-buckets}

A simple approach, which we empirically evaluate in Section~\ref{sec:experiments}, assigns each family to a separate external memory file.
Thus, the hash function extracts the first-$k$ subcombination of a node and directly maps that to a file on disk.
During the search, the files are expanded in lexicographic order of the corresponding first-$k$ subcombinations;
within the files, though, the nodes are unordered.
We refer to this strategy as \hhdd.

\subsection{Packing families into files}
\label{sec:bin-packing}

After choosing $k$ by solving Equation~\ref{eqn:min-k}, the \emph{maximum} number of nodes in any family is $C(n-k, l-k)$.
However, as mentioned in Section~\ref{sec:dividing-nodes}, nodes are distributed to the lexicographically first family to which they can belong.
For example, $\*F^{\{X_1, X_2\}}_3$ ``steals'' $\{X_1, X_2, X_3\}$ from $\*F^{\{X_1, X_3\}}_3$.
Therefore, $\*F^{\{X_1, X_3\}}_3$ will contain one node less than given by the bound.

In fact, because nodes are assigned to the lexicographically first family possible, \family~is assigned only nodes which follow \!S lexicographically.
This is, if we take $X_i$ as the lexicographically last element of \!S, then all extensions assigned to \family~use only variables $X_j$ such that $j > i$, other than those in \!S.
So, due to stealing, \family, with $X_i$ as the lexicographically last element, is assigned $C^\prime(n-i, l-k)$ nodes, where $C^\prime(n-i,l-k)$ is defined to be $0$ when $0 < n-i < l-k$ and the binomial coefficient otherwise.
We denote the number of nodes assigned to \family~as $|\family|$.

Furthermore, since the lexicographically last element of the subcombination for the family is $X_i$, the other $k-1$ elements must come from the $i-1$ elements which precede $X_i$.
Thus, $C(i-1, k-1)$ families end with $X_i$.

For a variety of reasons, such as operating system constraints on open file handles and latencies associated with  accessing many hard disk files, we aim to minimize the number of files used for storing the families.
Thus, we assign many families to a single file.
The constraint on assigning families is that the sum of the sizes of families assigned to a single file cannot exceed $M$.

Based on the previous analysis, the number of subcombinations and the number of items belonging to each subcombination is known.
Thus, we can solve a bin packing problem to assign families to files in order to minimize the number of required files.
Bin packing is known to be NP-hard~\cite{Garey1979}; 
however, a variety of efficient approximation algorithms are available for this problem~\cite{Johnson1973,Korf2002}.
We use Algorithm~\ref{alg:pack} for assigning families to files.

\begin{algorithm}[tb]
  \caption{Bin packing algorithm for packing families into files \label{alg:pack}}
  \begin{algorithmic}
    \Procedure{pack}{memory limit $M$, families \*F}
        \State $files \leftarrow \emptyset$             \Comment map from families to disk files
        \State $file \leftarrow \emptyset$              \Comment families are greedily added to the current file
        \For {\family~in \*F}                           \Comment iterate in reverse lexicographic order
            \If {$|\family| + \sum_{\!{S^\prime} \in file} |\*F^{\!{S^\prime}}_l| \leq M $}
                \State $file \leftarrow file \cup \family$
            \Else
                \State $files \leftarrow files \cup file$
                \State $file \leftarrow \family$
            \EndIf
        \EndFor 
        \State \Return $files$
    \EndProcedure
 \end{algorithmic}
\end{algorithm}

Thus, for this strategy, the hash function for distributing a node to disk first extracts the first-$k$ subcombination to find the family of the node.
Then, the results of the bin packing algorithm give the file to which that family is written.
The bin packing algorithm does not attempt to assign lexicographically similar families to the same files, so the order of node expansions is close to random.
We refer to this strategy as \hhddp.

%In Section~\ref{sec:experiments}, we use two different bin packing algorithms for assigning buckets to files.
%First, we use an optimal packing which guarantees to minimize the number of files.
%Second, we use the first-fit decreasing (FFD) approximation algorithm~\cite{Johnson1973}, which is known to be an $\frac{11}{9}$-approximation.

%In practice, for HDD search algorithms, all of the nodes in one bucket are expanded before moving to the next bucket.
%\textcolor{red}{This statement can be clarified more.}
%Consequently, a second goal of assigning buckets to files for HDD is to maximize locality by assigning lexicographically-similar subcombinations to the same bucket.
%To the best of our knowledge, no algorithms solve this problem.

%We do not explicitly incorporate locality into packing because that would lead to a multi-objective optimization problem.
%\textcolor{red}{This can be clarified more.}
%Nevertheless, in practice, FFD assigns buckets in lexicographic order.
%Thus, FFD implicitly uses a packing strategy that leads to high locality.

%\textcolor{red}{It may be possible to use the work on super-combinations by Tamada et al., to analytically calculate the amount of locality we get when assigning buckets to files.}



\section{Experiments}
\label{sec:experiments}

Locality is an important factor in the performance of HDD.
Thus, we designed a set of experiments to better understand how different HDD strategies affect locality.
%We first compared our greedy bin packing algorithm to existing approaches.
%Next, we investigated the effect of sorting compared to hashing for HDD.
%Finally, we studied how memory affected hHDD.
In particular, we tested the following three hypotheses.

\begin{enumerate}
    \item The proposed bin packing algorithm is comparable to state-of-the-art techniques.
    \item Given a fixed hash table size $m$, \shdd~has a higher locality than \hhdd~and \hhddp.
    \item Given a fixed amount of memory $M$, \hhdd~and \hhddp~have a higher locality than \shdd.
    \item For a fixed packing strategy, locality and $m$ are  positively correlated.
\end{enumerate}

\subsection{Datasets and environment}

All of the experiments were run on dual quad-core Intel Xeon E5540 processors with 32 GB of RAM.
Hard disk space was limited to 16 GB.

As described in Section~\ref{sec:state-space-search}, pruning is very important for the performance of BFBnB;
however, pruning is dependent both on the quality of the bound and the quality of the heuristic.
These are both data-dependent and difficult to analytically characterize.
Consequently, in order to remove this confounding factor from our analysis, we do not using pruning.

%We use datasets from a previous study~\cite{Malone2014}.
%Since we also do not use pruning, locality is the same for all datasets with the same number of variables for a fixed packing strategy.
%Thus, we only use one dataset for each number of variables.

Our analysis uses a $29$-variable dataset from a previous study~\cite{Malone2013}.
We do not use pruning, so the dataset generation parameters investigated in the previous study do not affect locality.
Since the locality behavior of the HDD strategies is completely deterministic, we report results on just one dataset.

\subsection{Bin packing performance}

We first evaluated the performance of the \hhddp~bin packing algorithm in Section~\ref{sec:bin-packing} by considering the number of files used by different strategies.
For comparison, we include the basic approach of assigning each family to a single file, \hhdd.
Additionally, we include the commonly-used first-fit decreasing approximation algorithm(\ffd)~\cite{Johnson1973}, which is known to be an $\frac{11}{9}$-approximation, and an optimal packing strategy (\opt)\footnote{The strategy is optimal in that it minimizes the number of files. We solve the optimization problem using an integer linear programming formulation.}.
Figure~\ref{fig:file-count} shows that the packing algorithm reduces the number of files by over an order of magnitude compared to \hhdd~across all bounds for $m$.
Indeed, the \hhddp~packing strategy only results in one extra file for two layers compared to \opt.
Thus, we conclude that our packing strategy is appropriate for reducing the number of files.

\begin{figure}[hp!]
    \centering
    \includegraphics[width=0.75\textwidth,keepaspectratio]{file-count-25M.eps}
    \includegraphics[width=0.75\textwidth,keepaspectratio]{file-count-50M.eps}
    \includegraphics[width=0.75\textwidth,keepaspectratio]{file-count-75M.eps}
    \caption{The number of files resulting from different bin packing strategies. There is only $1$ file for all strategies in the unshown layers.\label{fig:file-count}}
\end{figure}


\subsection{Locality, fixed hash table size}

We next compared the locality of \shdd, \hhdd~and \hhddp~when all algorithms are allowed to use the same maximum hash table size $m=25e6$.
Thus, due to the overhead of sorting, this experiment actually allows \shdd~to use more memory than the other approaches.
In Figure~\ref{fig:locality-fixed-hash-table}, we show how locality varies across layers in the order graph from $29$ variables.

As we expected, the locality is consistently higher for \shdd~compared to the other approaches.
We explain this behavior by noting that, by design, \shdd~expands nodes in sorted order.
Thus, it inherently exhibits more locality than the hashing-based HDD strategies.
On the other extreme, \hhddp~sacrifices locality by packing many families into the same file.
As mentioned in Section~\ref{sec:distributing-buckets}, \hhdd~expands the files in order of the first-$k$ subcombinations, but the nodes within the same family are unordered.
So it represents a compromise between the completely-sorted expansion strategy of \shdd~and the nearly-random strategy of \hhddp.
The figure shows that its locality is closer to that of \shdd~than \hhddp.


\begin{figure}[tb!]
    \centering
    \includegraphics[width=.75\textwidth,keepaspectratio]{locality-fixed-hash-table.eps}
    \caption{The locality of HDD algorithms for layers of the order graph for $29$ variables when the size of the hash table is $m=25e6$. The locality for all HDD algorithms is $1$ for all unshown layers. \label{fig:locality-fixed-hash-table}}
\end{figure}

\subsection{Locality, fixed maximum memory}

We then compared locality when holding the maximum memory requirement constant.
In particular, we used $M=25e6$;
thus, the size of the hash tables for \hhdd~and \hhddp~are $m=25e6$, while the size of the hash table for \shdd~is $m=1.7e6$ because of the \BigO{\log m} memory overhead for sorting.
Unsurprisingly, Figure~\ref{fig:locality-fixed-max-memory} shows the same relationship among \hhdd~and \hhddp~as in Figure~\ref{fig:locality-fixed-hash-table}.

Of course, due to the smaller hash table size, \shdd~exhibits worse locality for most layers than the other two strategies.
Unexpectedly, though, \shdd~has \emph{better} locality than \hhddp~for layers $13$ and $14$.
We again attribute this behavior to the locality inherent in expanding nodes in sorted order.

\begin{figure}[tb!]
    \centering
    \includegraphics[width=0.75\textwidth,keepaspectratio]{locality-fixed-max-memory.eps}
    \caption{The locality of HDD algorithms for layers of the order graph for $29$ variables when the maximum amount of memory is constant at $M=25e6$. For sHDD, $m=1.7e6$; for hHDD, $m=25e6$. The locality for all HDD algorithms is $1$ for all unshown layers. \label{fig:locality-fixed-max-memory}.}
\end{figure}

%\subsection{Packing efficiency}

\subsection{Locality and memory correlation}

We finally evaluated the relationship between the hash table size $m$ and locality for all three HDD strategies.
Figure~\ref{fig:locality-large-M} reveals several differences among the relationships for the three strategies.
Compared to the other HDD strategies, \hhddp~undergoes the most drastic locality changes as the available memory increases.
In particular, it exhibits an almost linear relationship between $m$ and locality.
We explain this behavior with the somewhat random order of node expansions due to packing;
in contrast, both \shdd~and \hhdd~inherently expand nodes in sorted order, so even a modest-sized hash table removes many of the duplicates.
Therefore, the locality inherent in these strategies reduces the need for larger hash tables.
Interestingly, for the largest layers ($14$ and $15$), the locality of \shdd~improves markedly from $m=25e6$ to $m=50e6$, but the improvement is less drastic when the size is again increased to $m=75e6$.
On the other hand, for \hhdd, there is little improvement from $m=25e6$ to $m=50e6$, but the locality significantly improves when the size of the hash table is increased to $m=75e6$.

\begin{figure}[tbhp!]
    \centering
    \includegraphics[width=0.75\textwidth,keepaspectratio]{locality-sort-large-M.eps}
    \includegraphics[width=0.75\textwidth,keepaspectratio]{locality-hash-large-M.eps}
    \includegraphics[width=0.75\textwidth,keepaspectratio]{locality-hash-bucket-large-M.eps}
    \caption{The locality of HDD algorithms for layers of the order graph for $29$ variables as the hash table size increases. The locality for all HDD algorithms is $1$ for all unshown layers. \label{fig:locality-large-M}}
\end{figure}

\section{Discussion}
\label{sec:discussion}

In this paper, we have presented a novel approach for hybrid duplicate detection using hashing-based delayed duplicate detection for solving  Bayesian network structure learning with breadth-first branch-and-bound search.
The main contribution of this work is a hash function which is used to distribute the nodes in the search space to files on disk;
importantly, the hash function  ensures that no single file contains more unique nodes than will fit in memory.
Compared to previous sorting-based techniques (\shdd), \hhdd~reduces the the memory complexity by a factor of \BigO{\log m}.
In practice, $m$ is on the order of tens to hundreds of millions, so the reduction is quite substantial.
Experimentally, we verified that \hhdd~can significantly improve locality and reduce the number of expensive external memory operations used for delayed duplicate detection.

Furthermore, we developed a bin packing algorithm for minimizing the number of external memory files.
Empirically, we showed that the bin packing approach often optimally minimizes the number of files, but sacrifices locality for doing so.
Minimizing the number of files and maximizing locality amounts to a dual-objective discrete optimization problem.
Nevertheless, locality-aware packing strategies could improve this behavior.
The hash function and bin packing algorithm could also be used to distribute nodes for parallel processing.




\bibliography{library}
\bibliographystyle{apalike}

\end{document}
