#! /usr/bin/env python3

import argparse
import utils

default_timeout = 15

def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description="This script finds the optimal number of bins for all of the provided "
        "variable counts and capacities.")
    parser.add_argument('variable_counts', help="File containing the variable counts")
    parser.add_argument('capacities', help="File containing the capacities")
    parser.add_argument('--timeout', help="The timeout to use for solving the ILP", 
        type=int, default=default_timeout)
    args = parser.parse_args()

    with open(args.variable_counts) as vc:
        variable_counts = [l.strip() for l in vc]

    with open(args.capacities) as c:
        capacities = [l.strip() for l in c]

    script = utils.add_home_dir('research', 'hash-based-ddd', 'bin-packing', 'run-optimal-bin-packing.py')

    for vc in variable_counts:
        for c in capacities:
            outfile = 'bins-{}-{}.out'.format(vc, c)
            outfile = utils.add_home_dir('research', 'hash-based-ddd', 'bin-packing', 'results', outfile)

            cmd = "{} {} {} --timeout {} > {}".format(script, vc, c, args.timeout, outfile)
            utils.check_call(cmd)

if __name__ == '__main__':
    main()

