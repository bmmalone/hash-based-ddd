#! /usr/bin/env python3

import argparse
import numpy as np
import scipy.misc
import scipy.optimize

import utils

default_zimpl_model = utils.add_home_dir('research', 'hash-based-ddd', 'bin-packing', 'optimal-bin-packing.zimpl')
default_timeout = 15
weights_filename_template = "weights-{}-{}-{}.txt"

def c_prime(n, i, l, k):
    if (n - i) < (l-k):
        return 0
    return int(scipy.misc.comb(n-i, l-k))

def get_k(n, l, M):
    for k in range(1, n+1):
        layer_size = scipy.misc.comb(n-k, l-k)
        if layer_size <= M:
            return k

def get_num_bins(n, l, M, zimpl_model, timeout):
    weights_filename = weights_filename_template.format(n, l, M)
    weights_filename = utils.add_home_dir('research', 'hash-based-ddd', 'bin-packing', 'weights', weights_filename)

    # first, find the smallest k that will work
    k = get_k(n, l, M)
    #print("The subcombinations are of size: {}".format(k))

    # there will be (n pick k) subcombinations
    num_nonzero_items = int(scipy.misc.comb(n,k))
    weights = np.zeros(num_nonzero_items)

    # get all of the bin sizes
    cur_weight_index = 0
    for i in range(k, n):
        num_bins_ending_with_i = int(scipy.misc.comb(i-1, k-1))
        bin_size = c_prime(n, i, l, k)

        # this should be fixed
        if bin_size == 0:
            weights = weights[:cur_weight_index]
            num_nonzero_items = cur_weight_index
            break

        if bin_size > M:
            # this should now never happen since we select K
            raise ValueError("The capacity is smaller than the size of the bins. capacity: {}, bin_size: {}".format(M, bin_size))

        weights[cur_weight_index:cur_weight_index + num_bins_ending_with_i] = bin_size
        cur_weight_index += num_bins_ending_with_i

    # use FFD to calculate how many bins we need

    # the weight currently in each bin
    ffd_bins = np.zeros(num_nonzero_items)
    for i in range(num_nonzero_items):
        # place item i in the first bin that can take it
        for j in range(num_nonzero_items):
            if ffd_bins[j] + weights[i] <= M:
                # then it fits, so increment the weight
                ffd_bins[j] += weights[i]

                # and move to the next item
                break

    ffd_num_bins = np.where(ffd_bins == 0)[0][0]

    #print("ffd_num_bins: {}".format(ffd_num_bins))

    if ffd_num_bins == 1:
        #print("FFD already uses only 1 bin. Quitting.")
        return (1,1)

    # write the zimple file
    with open(weights_filename, 'w') as out:
        out.write("# num_items\n{}\n".format(num_nonzero_items))
        out.write("# num_bins\n{}\n".format(ffd_num_bins))
        out.write("# capacity\n{}\n".format(M))

        out.write("# item weight\n")
        for i in range(num_nonzero_items):
            out.write("{} {}\n".format(i+1, weights[i]))

    zimpl_file = "{}.zimpl".format(weights_filename)
    cmd = "zimpl {} -D weights_file={} -o {}".format(zimpl_model, weights_filename, zimpl_file)
    utils.check_call(cmd)

    zimpl_lp_file = "{}.lp".format(zimpl_file)

    cmd = "timeout -k 1 {} scip -f {}".format(timeout, zimpl_lp_file)
    try:
        out = utils.check_output(cmd)
    except:
        # then the timeout (or something) happened
        # just ignore
        out = ""

    # parse out the objective value, which is the number of bins
    start_index = out.find("objective value:")

    # check if we timed out
    if start_index == -1:
        opt_num_bins = -1
    else:
        end_index = out.find("\n", start_index)
        substr = out[start_index:end_index]
        sp = substr.split()
        opt_num_bins = sp[-1]

    return (ffd_num_bins, opt_num_bins)

def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description="This script creates the ZIMPL input file for optimal bin packing for "
        "given parameters of the BNSL order graph.\n\nThe FFD algorithm is used to determine "
        "an upper bound on the number of bins.\n\nThis implementation is very inefficient.\n\n"
        "It then calls zimpl to create the lp file; finally, it calls scip to get the optimal "
        "number of bins.")
    parser.add_argument('n', type=int, help="Number of variables")
    parser.add_argument('capacity', type=int, help="The capacity of each bin")
    parser.add_argument('--zimpl-model', help="The zimpl model file",
        default=default_zimpl_model)
    parser.add_argument('--timeout', help="The time limit for solving the ips", type=int,
        default=default_timeout)
    args = parser.parse_args()

    print("run_opt_bin_packing_note,layer,ffd_num_bins,opt_num_bins")
    for l in range(1,args.n):
        (ffd_num_bins, opt_num_bins) = get_num_bins(args.n, l, args.capacity, args.zimpl_model, args.timeout)
        print("run_opt_bin_packing,{},{},{}".format(l, ffd_num_bins, opt_num_bins))

if __name__ == '__main__':
    main()
