param weights_file;

param num_items := read weights_file as "1n" use 1 comment "#";
param num_bins := read weights_file  as "1n" skip 1 use 1 comment "#";

set bins    := { 1 .. num_bins };
set items   := { 1 .. num_items };

param capacity  := read weights_file as "1n" skip 2 use 1 comment "#";
param w[items]  := read weights_file as "<1n> 2n" skip 3 comment "#";

var y[bins] binary;
var x[items * bins] binary;

minimize used_bins: sum <j> in bins: y[j];

subto bin_capacities: forall <j> in bins do
    sum <i> in items: w[i] * x[i,j] <= capacity;

subto placed_items: forall <i> in items do
    sum <j> in bins: x[i,j] == 1;

subto bin_in_use: forall <j> in bins do
    forall <i> in items: x[i,j] <= y[j];

subto symm_breaking_first_item:
    x[1, 1] == 1;

subto symm_use_earlier_bins:
    forall <j> in { 1 .. num_bins-1} do
        y[j] >= y[j+1];
